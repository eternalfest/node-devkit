import chai from "chai";
import { EditorService, withEditorProcess } from "../lib/editor";
import { readTextFile } from "./test-resources";

describe("editor", function () {
  describe("EditorProcess", function () {
    xit("should build a level", async function () {
      return withEditorProcess(async (editor: EditorService): Promise<void> => {
        const lvl2: string = await readTextFile("./editor/002.lvl");
        const result: any = await editor.buildLevel(lvl2);
        chai.assert.isTrue(result);
      });
    });
  });
});
