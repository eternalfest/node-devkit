import fs from "fs";
import path from "path";
import meta from "./meta.js";

function resolvePath(...segments: string[]): string {
  return path.resolve(meta.dirname, ...segments);
}

export async function readFile(path: string): Promise<Buffer> {
  return fs.promises.readFile(resolvePath(path));
}

export async function readTextFile(path: string): Promise<string> {
  return (await readFile(path)).toString("UTF-8");
}
