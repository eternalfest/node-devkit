import { exec as execTool, ExecOptions, ExecResult } from "../exec";
import { getLevelEditorPath } from "../paths";

export function getPath(): string {
  return getLevelEditorPath();
}

export function exec(options: ExecOptions): ExecResult {
  return execTool(getPath(), options);
}
