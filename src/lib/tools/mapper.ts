import { exec as execTool, ExecOptions, ExecResult } from "../exec";
import { getMapperPath } from "../paths";

export function getPath(): string {
  return getMapperPath();
}

export function exec(options: ExecOptions): ExecResult {
  return execTool(getPath(), options);
}
