import os from "os";
import path from "path";
import meta from "./meta.js";

function getDevkitRoot(): string {
  return path.resolve(meta.dirname, "devkit");
}

function getToolPath(toolName: string): string {
  if (os.platform() === "win32") {
    toolName += ".bat";
  }
  return path.resolve(getDevkitRoot(), toolName);
}

export function getDeobfuscatorPath(): string {
  return getToolPath("Deobfuscator");
}

export function getGameBundlerPath(): string {
  return getToolPath("GameBundler");
}

export function getLevelEditorPath(): string {
  return getToolPath("LevelEditor");
}

export function getLevelExtractorPath(): string {
  return getToolPath("LevelExtractor");
}

export function getMapperPath(): string {
  return getToolPath("Mapper");
}
