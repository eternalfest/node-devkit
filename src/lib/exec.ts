import childProcess from "child_process";
import process from "process";

export interface ExecOptions {
  args: string[];
  cwd: string;
  env?: NodeJS.ProcessEnv;
  stdio: "inherit";
}

export interface ExecResult {
  toPromise(): Promise<void>;
}

export function exec(filePath: string, options: ExecOptions): ExecResult {
  const process: childProcess.ChildProcess = childProcess.spawn(
    filePath,
    options.args,
    {
      stdio: options.stdio,
      cwd: options.cwd,
      env: getAllEnvVariables(options.env),
    },
  );

  let closed: boolean = false;
  let code: number | null;

  process.once("exit", (_code, _signal) => {
    code = _code;
    closed = true;
  });

  return {
    async toPromise(): Promise<void> {
      return new Promise<void>((resolve, reject) => {
        function onClosed(code: number | null): void {
          if (code !== 0) {
            reject(new Error("Process did not exit with code 0"));
          } else {
            resolve();
          }
        }

        if (closed) {
          onClosed(code);
        } else {
          process.once("exit", (code, _signal) => {
            onClosed(code);
          });
        }
      });
    },
  };
}

// Returns the env variables in `extra`, merged with the process' env variables.
// The process' env variables take priority over the extra ones.
function getAllEnvVariables(extra?: NodeJS.ProcessEnv): NodeJS.ProcessEnv {
  if (extra === undefined) {
    return process.env;
  }
  const result: NodeJS.ProcessEnv = { ...extra };
  Object.assign(result, process.env);
  return result;
}
