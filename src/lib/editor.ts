// tslint:disable:no-use-before-declare
import childProcess from "child_process";
import { Incident } from "incident";
import { getLevelEditorPath } from "./paths";

export interface EditorService {
  buildLevel(lvlSource: string): Promise<any>;
}

export async function withEditorProcess<R = void>(handler: (server: EditorService) => Promise<R>): Promise<R> {
  const process: EditorProcess = EditorProcess.spawn();
  let result: R;
  try {
    result = await handler(process);
  } catch (err) {
    await process.end();
    throw err;
  }
  await process.end();
  return result;
}

export interface SpawnOptions {
  cwd?: string;
  env?: {[key: string]: string};
  stdio?: "inherit" | "pipe";
  /**
   * Run in detached mode. Default: `false`.
   */
  detached?: boolean;
}

export interface SpawnResult {
  /**
   * Buffer containing the whole standard output of the spawned process.
   */
  stdout: Buffer;

  /**
   * Buffer containing the whole standard error of the spawned process.
   */
  stderr: Buffer;

  /**
   * Exit value of the spawned process: a return code or exit signal.
   */
  exit: Exit;
}

/**
 * Exit value of a spawned process: a return code or exit signal
 */
export type Exit = SignalExit | CodeExit;

export interface CodeExit {
  type: "code";
  code: number;
}

export interface SignalExit {
  type: "signal";
  signal: string;
}

interface DeferredPromise<R> {
  resolve(result: R): void;

  reject(cause: Error): void;
}

enum EditorProcessState {
  Running,
  WaitForExit,
  Exited,
}

/*
 * FSM (default: WaitingForRequest):
 * WaitingForRequest(stdout) -> Abort
 * WaitingForRequest(stderr) -> Abort
 * WaitingForRequest(exit) -> Exit
 * WaitingForRequest(request) -> WaitingForResults
 */
class EditorProcess implements EditorService {
  private readonly process: childProcess.ChildProcess;
  private readonly stdOutChunks: Buffer[];
  private readonly stdErrChunks: Buffer[];
  private readonly buildLevelRequests: DeferredPromise<any>[];
  private readonly waitForExitRequests: DeferredPromise<any>[];
  private state: EditorProcessState;

  private constructor() {
    this.buildLevelRequests = [];
    this.waitForExitRequests = [];
    this.stdOutChunks = [];
    this.stdErrChunks = [];

    this.process = childProcess.spawn(getLevelEditorPath(), ["--background"]);
    this.process.once("exit", (code: number | null, signal: string | null): void => {
      this.handleExit(code !== null ? {type: "code", code} : {type: "signal", signal: signal!});
    });
    this.process.stdout!.on("data", this.handleStdOutData.bind(this));
    this.process.stderr!.on("data", this.handleStdErrData.bind(this));
    this.state = EditorProcessState.Running;
  }

  static spawn(): EditorProcess {
    return new EditorProcess();
  }

  async buildLevel(lvlSource: string): Promise<any> {
    const BUILD_LEVEL_TIMEOUT: number = 1000;

    if (this.state === EditorProcessState.Exited) {
      throw new Incident("EditorProcessExited");
    }
    return new Promise((resolve, reject) => {
      const req: DeferredPromise<any> = {resolve, reject};
      this.buildLevelRequests.push(req);
      setTimeout(() => {
        // Remove from queue and fail if it's there after the timeout
        const reqIndex: number = this.buildLevelRequests.indexOf(req);
        if (reqIndex >= 0) {
          this.buildLevelRequests.splice(reqIndex, 1);
          req.reject(new Incident("BuildLevelTimeout"));
          this.kill();
        }
      }, BUILD_LEVEL_TIMEOUT);

      const srcBuffer: Buffer = Buffer.from(lvlSource.trim());
      // this.process.stdin.write(`${srcBuffer.length}\n`);
      this.process.stdin!.write(srcBuffer);
      this.process.stdin!.write("\n");
      // The promised will be resolved in the stdout handler.
    });
  }

  async end(): Promise<void> {
    this.kill();
    return this.waitForExit();
  }

  private kill(): void {
    if (this.state === EditorProcessState.Running) {
      this.process.kill();
      this.state = EditorProcessState.WaitForExit;
    }
  }

  private async waitForExit(): Promise<void> {
    if (this.state !== EditorProcessState.Exited) {
      return new Promise<void>((resolve, reject) => {
        this.waitForExitRequests.push({resolve, reject});
      });
    }
  }

  private handleStdOutData(chunk: Buffer): void {
    this.stdOutChunks.push(chunk);
    if (this.state !== EditorProcessState.Running) {
      this.state = EditorProcessState.WaitForExit;
      return;
    }
    this.processResponses();
  }

  private handleStdErrData(chunk: Buffer): void {
    this.stdErrChunks.push(chunk);
    this.state = EditorProcessState.WaitForExit;
  }

  private processResponses() {
    let stdOut: Buffer = Buffer.concat(this.stdOutChunks);
    while (this.buildLevelRequests.length > 0) {
      const eol: [number, number] | undefined = findEol(stdOut);
      if (eol === undefined) {
        break;
      }
      const buffer: Buffer = stdOut.slice(0, eol[0]);
      stdOut = stdOut.slice(eol[0] + eol[1]);
      const req: DeferredPromise<any> = this.buildLevelRequests.shift()!;
      try {
        const response: any = JSON.parse(buffer.toString("UTF-8"));
        req.resolve(response);
      } catch (err) {
        req.reject(new Incident(err, "UnexpectedEditorResponse", {buffer}));
      }
    }
    this.stdOutChunks.length = 0;
    this.stdOutChunks.push(stdOut);
  }

  private handleExit(_exit: Exit): void {
    const stdErr: string = Buffer.concat(this.stdErrChunks).toString("UTF-8");
    if (stdErr !== "") {
      const nextReq: DeferredPromise<any> | undefined = this.buildLevelRequests.shift();
      if (nextReq !== undefined) {
        this.stdErrChunks.length = 0;
        nextReq.reject(new Incident("BuildLevel", stdErr));
      } else {
        // TODO: Find a better way to handle orphan errors
        console.warn("UnhandledEditorStdErr");
        console.warn({stdErr});
      }
    }
    const stdOut: string = Buffer.concat(this.stdOutChunks).toString("UTF-8");
    if (stdOut !== "") {
      console.warn("UnhandledEditorStdOut");
      console.warn({stdOut});
    }

    for (const req of this.waitForExitRequests) {
      req.resolve(undefined);
    }
    this.waitForExitRequests.length = 0;

    for (const req of this.buildLevelRequests) {
      req.reject(new Incident("EditorProcessExited"));
    }
    this.waitForExitRequests.length = 0;

    this.state = EditorProcessState.Exited;
    this.process.stdout!.removeAllListeners("data");
    this.process.stderr!.removeAllListeners("data");
  }
}

/**
 * @return `undefined` if not found, else `[startPos, length]`
 */
function findEol(buffer: Uint8Array): [number, number] | undefined {
  const lfIndex: number = buffer.indexOf(chars.LINE_FEED);
  if (lfIndex < 0) {
    return undefined;
  } else if (lfIndex > 0 && buffer[lfIndex - 1] === chars.CARRIAGE_RETURN) {
    return [lfIndex - 1, 2];
  } else {
    return [lfIndex, 1];
  }
}

namespace chars {
  export const CARRIAGE_RETURN: 0x0d = 0x0d;
  export const LINE_FEED: 0x0a = 0x0a;
}
