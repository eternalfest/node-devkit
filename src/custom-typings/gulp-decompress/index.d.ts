declare module "gulp-decompress" {
  namespace gulpDecompress {
    export type Decompress = any;
  }

  const gulpDecompress: gulpDecompress.Decompress;

  export = gulpDecompress;
}
