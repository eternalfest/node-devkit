# 2.0.1 (2023-02-25)

- **[Fix]** Update devkit to version from 2023-02-25.

# 2.0.0 (2023-02-09)

- **[Breaking change]** Update devkit to version from 2023-02-09.
- **[Breaking change]** Remove obsolete tools (game-bundler, deobfuscator, level-extractor).
- **[Breaking change]** Remove CLI entrypoint.

# 1.0.25 (2020-04-23)

- **[Fix]** Update devkit to version from 2020-04-23.

# 1.0.24 (2020-01-31)

- **[Feature]** Add optional `env` argument to `ExecOptions`.
- **[Fix]** Update devkit to version from 2020-01-30.

# 1.0.23 (2020-01-22)

- **[Fix]** Update devkit to version from 2020-01-20.

# 1.0.22 (2020-01-01)

- **[Fix]** Update devkit to version from 2019-12-31.
- **[Internal]** Add `update-submodules.sh`.
- **[Internal]** Update dev dependencies.

# 1.0.21 (2019-08-31)

- **[Fix]** Update devkit to version from 2019-08-31.

# 1.0.20 (2019-08-25)

- **[Fix]** Update build tools.
- **[Fix]** Update devkit to version from 2019-08-25.

# 1.0.19 (2019-05-14)

- **[Fix]** Update devkit to version from 2019-05-14.

# 1.0.18 (2019-05-06)

- **[Fix]** Update devkit to version from 2019-05-06.

# 1.0.17 (2019-05-03)

- **[Fix]** Update devkit to version from 2019-05-03.

# 1.0.16 (2019-05-02)

- **[Fix]** Update devkit to version from 2019-05-02.

# 1.0.15 (2019-04-28)

- **[Fix]** Update devkit to version from 2019-04-28.

# 1.0.14 (2019-04-15)

- **[Fix]** Update devkit to version from 2019-04-14.

# 1.0.13 (2019-04-13)

- **[Fix]** Update devkit to version from 2019-04-13.

# 1.0.12 (2019-04-13)

- **[Fix]** Update devkit to version from 2019-04-13.

# 1.0.11 (2019-04-04)

- **[Fix]** Update devkit to version from 2019-04-04.

# 1.0.10 (2019-04-02)

- **[Fix]** Update devkit to version from 2019-04-02.

# 1.0.9 (2019-04-01)

- **[Fix]** Update devkit to version from 2019-03-29.

# 1.0.8 (2019-03-05)

- **[Fix]** Update devkit to version from 2019-03-05.
- **[Fix]** Update dependencies

# 1.0.7 (2019-02-05)

- **[Fix]** Update devkit to version from 2019-01-25.

# 1.0.6 (2019-01-23)

- **[Fix]** Update devkit to version from 2019-01-23.

# 1.0.5 (2018-12-21)

- **[Fix]** Update devkit to support multiple items per level.
- **[Internal]** Move `devkit` submodule to `deps/devkit`.
- **[Internal]** Update build tools.

# 1.0.4 (2018-11-21)

- **[Fix]** Update devkit to fix serialization of fields.

# 1.0.3 (2018-11-13)

- **[Fix]** Update bundler to fix generation of games with a `lang.xml` file.

# 1.0.2 (2018-10-16)

- **[Fix]** Update editor to fix more issues when loading levels with enemies.

# 1.0.1 (2018-10-16)

- **[Fix]** Update editor to fix issue when loading levels with enemies.

# 1.0.0 (2018-10-12)

- **[Feature]** Update bundled editor: add support for per-project assets.

# 1.0.0-rc.3 (2017-07-23)

- **[Fix]** Add support for Windows.

# 1.0.0-rc.2

- **[Breaking]** Use the stable `devkit` branch (with Gradle support). The tools were
  renamed accordingly.
- **[Breaking]** The signature of the `<tool>.exec` function changed to accept a single
  options object with the arguments. The current working directory and stdio policy
  are now required.

# 1.0.0-rc.1

- **[Internal]** Use dedicate repository for the `node-devkit` package.
  The original repo (`devkit`) now only contains Java files.
- **[Internal]** Do not publish the source-code of the devkit to npm.
- **[Breaking]** Namespace the function by tool name.

# 1.0.0-rc.0

- **[Feature]** Create npm package for the Devkit
