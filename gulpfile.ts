import childProcess from "child_process";
import gulp from "gulp";
import gulpDecompress from "gulp-decompress";
import minimist from "minimist";
import sysPath from "path";
import * as buildTools from "turbo-gulp";
import { Project } from "turbo-gulp/project";
import { LibTarget, registerLibTasks } from "turbo-gulp/targets/lib";
import { MochaTarget, registerMochaTasks } from "turbo-gulp/targets/mocha";

function exec(name: string, args: string[], opts: childProcess.SpawnOptions) {
  return new Promise((resolve, reject) => {
    const process: childProcess.ChildProcess = childProcess.spawn(name, args, {stdio: "inherit", ...opts});
    process.once("exit", (code, signal) => {
      if (code !== 0) {
        reject(new Error("Process did not exit with code 0"));
      } else {
        resolve();
      }
    });
  });
}

gulp.task(
  "devkit:build",
  () => exec("./gradlew", ["-p", "devkit", "distZip"], {cwd: sysPath.resolve(__dirname, "deps", "devkit")}),
);

gulp.task(
  "devkit:import",
  () => {
    return gulp
      .src(
        sysPath.resolve(__dirname, "deps", "devkit", "devkit", "build", "distributions", "devkit-*.zip"),
        {base: sysPath.resolve(__dirname, "deps", "devkit", "devkit", "build", "distributions")},
      )
      .pipe(gulpDecompress({strip: 1}))
      .pipe(gulp.dest(sysPath.resolve(__dirname, "build", "devkit")));
  });

gulp.task("devkit", gulp.series("devkit:build", "devkit:import"));

interface Options {
  next?: string;
}

const options: Options & minimist.ParsedArgs = minimist(process.argv.slice(2), {
  string: ["next"],
  default: {next: undefined},
});

const project: Project = {
  root: __dirname,
  packageJson: "package.json",
  buildDir: "build",
  distDir: "dist",
  srcDir: "src",
};

const lib: LibTarget = {
  project,
  name: "lib",
  srcDir: "src/lib",
  scripts: ["**/*.ts"],
  mainModule: "index",
  dist: {
    packageJsonMap: (old: buildTools.PackageJson): buildTools.PackageJson => {
      const version: string = options.next !== undefined ? `${old.version}-build.${options.next}` : old.version;
      return <any> {...old, version, scripts: undefined, private: false};
    },
    npmPublish: {
      tag: options.devDist !== undefined ? "next" : "latest",
    },
  },
  customTypingsDir: "src/custom-typings",
  tscOptions: {
    declaration: true,
    skipLibCheck: true,
  },
  typedoc: {
    dir: "typedoc",
    name: "Node Devkit",
  },
  copy: [
    {
      src: "../../build/devkit",
      files: ["**/*"],
      dest: "devkit",
    },
  ],
  clean: {
    dirs: ["build/lib", "dist/lib"],
  },
};

const test: MochaTarget = {
  project,
  name: "test",
  srcDir: "src",
  scripts: ["test/**/*.ts", "lib/**/*.ts"],
  customTypingsDir: "src/custom-typings",
  tscOptions: {
    skipLibCheck: true,
  },
  copy: [
    {
      src: "../build/devkit",
      files: ["**/*"],
      dest: "lib/devkit",
    },
    {files: ["test/editor/**/*.lvl"]},
  ],
  clean: {
    dirs: ["build/test"],
  },
};

const libTasks: any = registerLibTasks(gulp, lib);
registerMochaTasks(gulp, test);
buildTools.projectTasks.registerAll(gulp, project);

gulp.task("all:tsconfig.json", gulp.parallel("lib:tsconfig.json", "test:tsconfig.json"));
gulp.task("dist", libTasks.dist);
gulp.task("default", libTasks.dist);
